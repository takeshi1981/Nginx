# Memo about nginx process

- 稼働しているプロセスの確認

      nproc
      lscpu

- プロセスが同時にオープンできるファイル数

      ulimit -n

※ulimitコマンド  
<https://www.atmarkit.co.jp/ait/articles/1908/01/news031.html>

- 最大コネクション数

      worker_processes x worker_connections = max connections

- モジュールを追加する際に使用可能なモジュールを調べる方法

      .configure --help
